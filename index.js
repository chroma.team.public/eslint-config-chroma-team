"use strict";

module.exports = {
  extends: [
    "eslint:recommended",
    "plugin:json/recommended"
  ],
  rules: {
    "keyword-spacing": "error",
    "no-unreachable": "warn",
    "no-unused-vars": "warn",
    "require-atomic-updates": "warn",
    "no-whitespace-before-property": "error",
    "no-var": "error",
    "no-param-reassign": "error",
    "prefer-arrow-callback": "error",
    "arrow-parens": "error",
    "arrow-spacing": "error",
    "no-useless-computed-key": "error",
    "object-shorthand": [
      "error",
      "always"
    ],
    "object-curly-spacing": [
      "error",
      "always"
    ],
    "rest-spread-spacing": "error",
    "no-useless-rename": "error",
    "prefer-const": "error",
    "no-unneeded-ternary": "error",
    "default-case": "error",
    "default-param-last": "error",
    eqeqeq: "error",
    "no-return-assign": "error",
    yoda: "error",
    semi: "error",
    quotes: [
      "error",
      "double"
    ],
    "quote-props": [
      "error",
      "as-needed"
    ],
    "comma-dangle": "error",
    "comma-style": "error",
    "func-call-spacing": "error",
    "array-bracket-newline": [
      "error",
      "consistent"
    ],
    indent: [
      "error",
      2,
      {
        SwitchCase: 1
      }
    ],
    "block-spacing": "error",
    "no-multiple-empty-lines": ["error", { max: 1 }],
    "space-before-blocks": ["error", "always"],
    "no-trailing-spaces": "error",
    "no-multi-spaces": "error",
		"space-in-parens": "error",
		"space-infix-ops": "error",
		"key-spacing": [
			"error",
			{
				"afterColon": true
			}
		],
    "space-before-function-paren": [
      "error",
      {
        anonymous: "never",
        named: "never",
        asyncArrow: "always"
      }
    ],
    "spaced-comment": [
      "error",
      "always",
      {
        block: {
          exceptions: [
            "*"
          ]
        },
        line: {
          exceptions: [
            "/"
          ]
        }
      }
    ],
    "padded-blocks": ["error", "never"],
    "comma-spacing": [
			"error",
			{
				"before": false,
				"after": true
			}
		]
  },
  parserOptions: {
    ecmaVersion: 11
  },
  env: {
    es6: true,
    browser: true,
    node: true,
    mocha: true
  }
};
